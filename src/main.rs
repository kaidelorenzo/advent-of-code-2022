use pathfinding::prelude::bfs;
use std::cmp::max;
use std::cmp::Ordering;
use std::collections::hash_map::Entry::Vacant;
use std::collections::hash_map::RandomState;
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::env;
use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::iter::FromIterator;
use std::path::PathBuf;

struct TopThree {
    first: i32,
    second: i32,
    third: i32,
}

impl TopThree {
    pub fn add(&mut self, count: i32) {
        if count > self.first {
            self.third = self.second;
            self.second = self.first;
            self.first = count;
        } else if count > self.second {
            self.third = self.second;
            self.second = count;
        } else if count > self.third {
            self.third = count
        }
    }
    pub fn sum(&self) -> i32 {
        self.first + self.second + self.third
    }
}

struct Processor {
    current_letters: VecDeque<char>,
    letters_set: HashSet<char>,
    letter_map: HashMap<char, i32>,
}

impl Processor {
    pub fn push(&mut self, letter: char) {
        self.current_letters.push_back(letter);
        if let Vacant(e) = self.letter_map.entry(letter) {
            self.letters_set.insert(letter);
            e.insert(1);
        } else {
            self.letter_map
                .entry(letter)
                .and_modify(|count| *count += 1);
        }
    }
    pub fn pop(&mut self) {
        let popped = self.current_letters.pop_front().unwrap();
        if self.letter_map[&popped] == 1 {
            self.letters_set.remove(&popped);
            self.letter_map.remove(&popped);
        } else {
            self.letter_map
                .entry(popped)
                .and_modify(|count| *count -= 1);
        }
    }
    pub fn count_unique(&self) -> usize {
        self.letters_set.len()
    }
}

#[derive(Debug)]
struct Directory {
    total_size: usize,
    sub_dirs_unaccounted_for: usize,
}

#[derive(Copy, Clone)]
struct Visible {
    visible: bool,
    tallest: u32,
}

#[derive(Copy, Clone)]
struct Tree {
    right: Visible,
    left: Visible,
    top: Visible,
    bottom: Visible,
    height: u32,
    visible: bool,
}

#[derive(Copy, Clone)]
struct Visible2 {
    scores: [usize; 10],
}

#[derive(Copy, Clone)]
struct Tree2 {
    right: Visible2,
    left: Visible2,
    top: Visible2,
    bottom: Visible2,
    height: usize,
}

struct Monkey {
    items: VecDeque<i32>,
    function: Box<dyn Fn(i32) -> i32>,
    denominator: i32,
    true_monkey: usize,
    false_monkey: usize,
    items_inspected: usize,
}

struct Monkey2 {
    items: VecDeque<u64>,
    function: Box<dyn Fn(u64, u64) -> u64>,
    denominator: u64,
    true_monkey: usize,
    false_monkey: usize,
    items_inspected: usize,
}

/*
enum Cell {
    Air,
    Sand,
}
*/

fn run() {
    day1();
    day2();
    day3();
    day4();
    day5();
    day6();
    day7();
    day8();
    day9();
    day10();
    day11();
    day12();
    day13();
    day14()
}

fn main() {
    run()
}

fn day14() {
    // 0 through 159 rows
    // 421 through 508
    /*
    let reader = load_puzzle("day13.txt");
    let mut cave: Vec<Vec<Cell>> = Vec::<Vec<Cell>>::new();
    for line in reader.lines().map(|line| line.unwrap().split(" -> ").collect::<Vec<&str>>()){
        for i in 0..=(line.len()-2) {
            let point1 = line[i].split(",").map(|num| num.parse::<i32>().unwrap()).collect::<Vec<i32>>();
            let point2 = line[i+1].split(",").map(|num| num.parse::<i32>().unwrap()).collect::<Vec<i32>>();
            for x in point1[0]..=point2[0]{
                for y in point1[1]..=point2[1]{
                    // fill out vector
                }
            }
        }
    }
    */
}

fn day13() {
    let reader = load_puzzle("day13.txt");
    let mut lines = reader.lines();
    let mut current_pair = 0;
    let mut sum = 0;
    while let (Some(line1), Some(line2), Some(_line3)) = (lines.next(), lines.next(), lines.next())
    {
        current_pair += 1;
        let line1 = line1.unwrap();
        let mut line1_chars = line1.chars().peekable();
        let line2 = line2.unwrap();
        let mut line2_chars = line2.chars().peekable();
        line1_chars.next().unwrap();
        line2_chars.next().unwrap();

        //let mut level1 = 1;
        //let mut level2 = 1;
        let mut correct_order = None;

        let mut char1 = line1_chars.next().unwrap().to_string();
        let mut char2 = line2_chars.next().unwrap().to_string();
        //let mut next_action =
        loop {
            match (char1.as_str(), char2.as_str()) {
                (",", ",") => {
                    char1 = line1_chars.next().unwrap().to_string();
                    char2 = line2_chars.next().unwrap().to_string()
                }
                ("[", "[") => {
                    char1 = line1_chars.next().unwrap().to_string();
                    char2 = line2_chars.next().unwrap().to_string()
                }
                (_, "[") => {
                    //level1 += 1;
                    char2 = line2_chars.next().unwrap().to_string()
                }
                ("[", _) => {
                    char1 = line1_chars.next().unwrap().to_string();
                    //level2 += 1
                }
                ("]", "]") => {
                    char1 = match line1_chars.next() {
                        Some(char) => char.to_string(),
                        None => {
                            correct_order = Some(true);
                            break;
                        }
                    };
                    char2 = match line2_chars.next() {
                        Some(char) => char.to_string(),
                        None => {
                            correct_order = Some(false);
                            break;
                        }
                    };
                }
                ("]", _) => correct_order = Some(true),
                (_, "]") => correct_order = Some(false),
                (_, _) if line1_chars.peek().unwrap().is_ascii_digit() => {
                    char1 += &line1_chars.next().unwrap().to_string()
                }
                (_, _) if line2_chars.peek().unwrap().is_ascii_digit() => {
                    char2 += &line2_chars.next().unwrap().to_string()
                }
                (_, _) if char1.parse::<i32>().unwrap() < char2.parse::<i32>().unwrap() => {
                    correct_order = Some(true)
                }
                (_, _) if char1.parse::<i32>().unwrap() > char2.parse::<i32>().unwrap() => {
                    correct_order = Some(false)
                }
                (_, _) if char1.parse::<i32>().unwrap() == char2.parse::<i32>().unwrap() => {
                    char1 = line1_chars.next().unwrap().to_string();
                    char2 = line2_chars.next().unwrap().to_string()
                }
                (_, _) => panic!(),
            }
            if correct_order.is_some() {
                break;
            }
        }
        if correct_order.unwrap() {
            sum += current_pair;
        }
    }
    println!("{sum}")
}

fn day12() {
    let reader = load_puzzle("day12.txt");
    let mut map = Vec::<Vec<u8>>::new();
    let mut start: (usize, usize) = (0, 0);
    let mut end: (usize, usize) = (0, 0);
    for (line_num, line) in reader.lines().enumerate() {
        map.push(
            line.unwrap()
                .chars()
                .enumerate()
                .map(|(column, char)| match char {
                    'S' => {
                        start = (line_num, column);
                        b'a'
                    }
                    'E' => {
                        end = (line_num, column);
                        b'z'
                    }
                    _ => char as u8,
                })
                .collect::<Vec<u8>>(),
        );
    }
    let result = bfs(&start, |p| successors(map.as_slice(), *p), |p| *p == end);
    let length = result.unwrap().len() - 1;
    assert_eq!(length, 383);
    println!("{length}");
    let result = bfs(
        &end,
        |p| other_successors(map.as_slice(), *p),
        |p| map[p.0][p.1] == 97,
    );
    let length = result.unwrap().len() - 1;
    assert_eq!(length, 377);
    println!("{}", length)
}

fn other_successors(map: &[Vec<u8>], p: (usize, usize)) -> Vec<(usize, usize)> {
    let mut successors = Vec::<(usize, usize)>::new();
    if p.1 > 0 && map[p.0][p.1 - 1] + 1 >= map[p.0][p.1] {
        successors.push((p.0, p.1 - 1));
    }
    if p.1 < map[0].len() - 1 && map[p.0][p.1 + 1] + 1 >= map[p.0][p.1] {
        successors.push((p.0, p.1 + 1));
    }
    if p.0 > 0 && map[p.0 - 1][p.1] + 1 >= map[p.0][p.1] {
        successors.push((p.0 - 1, p.1));
    }
    if p.0 < map.len() - 1 && map[p.0 + 1][p.1] + 1 >= map[p.0][p.1] {
        successors.push((p.0 + 1, p.1));
    }
    successors
}

fn successors(map: &[Vec<u8>], p: (usize, usize)) -> Vec<(usize, usize)> {
    let mut successors = Vec::<(usize, usize)>::new();
    if p.1 > 0 && map[p.0][p.1 - 1] <= map[p.0][p.1] + 1 {
        successors.push((p.0, p.1 - 1));
    }
    if p.1 < map[0].len() - 1 && map[p.0][p.1 + 1] <= map[p.0][p.1] + 1 {
        successors.push((p.0, p.1 + 1));
    }
    if p.0 > 0 && map[p.0 - 1][p.1] <= map[p.0][p.1] + 1 {
        successors.push((p.0 - 1, p.1));
    }
    if p.0 < map.len() - 1 && map[p.0 + 1][p.1] <= map[p.0][p.1] + 1 {
        successors.push((p.0 + 1, p.1));
    }
    successors
}

fn day11() {
    let reader = load_puzzle("day11.txt");
    let mut lines = reader.lines();
    let mut monkeys = Vec::<Monkey>::new();
    while let (Some(_line1), Some(line2), Some(line3), Some(line4), Some(line5), Some(line6)) = (
        lines.next(),
        lines.next(),
        lines.next(),
        lines.next(),
        lines.next(),
        lines.next(),
    ) {
        let line3 = line3.unwrap();
        let operation = line3
            .strip_prefix("  Operation: new = ")
            .unwrap()
            .split(' ')
            .collect::<Vec<&str>>();
        let left = operation[0].to_owned();
        let right = operation[2].to_owned();
        let function: Box<dyn Fn(i32) -> i32> = match (operation[0], operation[2]) {
            ("old", "old") => match operation[1] {
                "+" => Box::new(move |num| num + num),
                "*" => Box::new(move |num| num * num),
                _ => panic!(),
            },
            ("old", _) => match operation[1] {
                "+" => Box::new(move |num| num + right.parse::<i32>().unwrap()),
                "*" => Box::new(move |num| num * right.parse::<i32>().unwrap()),
                _ => panic!(),
            },
            (_, "old") => match operation[1] {
                "+" => Box::new(move |num| left.parse::<i32>().unwrap() + num),
                "*" => Box::new(move |num| left.parse::<i32>().unwrap() * num),
                _ => panic!(),
            },
            (_, _) => match operation[1] {
                "+" => Box::new(move |_num| {
                    left.parse::<i32>().unwrap() + right.parse::<i32>().unwrap()
                }),
                "*" => Box::new(move |_num| {
                    left.parse::<i32>().unwrap() * right.parse::<i32>().unwrap()
                }),
                _ => panic!(),
            },
        };
        let monkey = Monkey {
            items: line2
                .unwrap()
                .strip_prefix("  Starting items: ")
                .unwrap()
                .split(", ")
                .map(|num| num.parse::<i32>().unwrap())
                .collect::<VecDeque<i32>>(),
            function,
            denominator: line4
                .unwrap()
                .strip_prefix("  Test: divisible by ")
                .unwrap()
                .parse::<i32>()
                .unwrap(),
            true_monkey: line5
                .unwrap()
                .strip_prefix("    If true: throw to monkey ")
                .unwrap()
                .parse::<usize>()
                .unwrap(),
            false_monkey: line6
                .unwrap()
                .strip_prefix("    If false: throw to monkey ")
                .unwrap()
                .parse::<usize>()
                .unwrap(),
            items_inspected: 0,
        };
        lines.next();
        monkeys.push(monkey)
    }
    let mut moved_items = Vec::<VecDeque<i32>>::new();
    for _monkey in 0..monkeys.len() {
        moved_items.push(VecDeque::<i32>::new());
    }
    let mut top = 0;
    let mut second = 0;
    for _round in 1..=20 {
        //for monkey in &mut monkeys {
        for monkey in 0..monkeys.len() {
            for item in &monkeys[monkey].items {
                //  Monkey inspects an item with a worry level of 79.
                // Worry level is multiplied by 19 to 1501.
                // Monkey gets bored with item. Worry level is divided by 3 to 500.
                // Current worry level is not divisible by 23.
                // Item with worry level 500 is thrown to monkey 3.
                let new_worry = (monkeys[monkey].function)(*item) / 3;
                if new_worry % monkeys[monkey].denominator == 0 {
                    let monkey_destination = monkeys[monkey].true_monkey;
                    moved_items[monkey_destination].push_back(new_worry);
                } else {
                    let monkey_destination = monkeys[monkey].false_monkey;
                    moved_items[monkey_destination].push_back(new_worry);
                }
            }
            monkeys[monkey].items_inspected += monkeys[monkey].items.len();
            monkeys[monkey].items.clear();
            for (monkey, item_list) in moved_items.iter_mut().enumerate() {
                for item in &mut *item_list {
                    monkeys[monkey].items.push_back(*item);
                }
                item_list.clear();
            }
        }
    }
    for monkey in monkeys {
        if monkey.items_inspected > top {
            second = top;
            top = monkey.items_inspected;
        } else if monkey.items_inspected > second {
            second = monkey.items_inspected;
        }
    }
    assert_eq!(top * second, 98280);
    println!("{}", top * second);
    let reader = load_puzzle("day11.txt");
    let mut lines = reader.lines();
    let mut monkeys = Vec::<Monkey2>::new();
    while let (Some(_line1), Some(line2), Some(line3), Some(line4), Some(line5), Some(line6)) = (
        lines.next(),
        lines.next(),
        lines.next(),
        lines.next(),
        lines.next(),
        lines.next(),
    ) {
        let line3 = line3.unwrap();
        let operation = line3
            .strip_prefix("  Operation: new = ")
            .unwrap()
            .split(' ')
            .collect::<Vec<&str>>();
        let left = operation[0].to_owned();
        let right = operation[2].to_owned();
        let function: Box<dyn Fn(u64, u64) -> u64> = match (operation[0], operation[2]) {
            ("old", "old") => match operation[1] {
                "+" => Box::new(move |num, _shrink| num + num),
                "*" => Box::new(move |num, shrink| (num % shrink) * (num % shrink)),
                _ => panic!(),
            },
            ("old", _) => match operation[1] {
                "+" => Box::new(move |num, _shrink| num + right.parse::<u64>().unwrap()),
                "*" => Box::new(move |num, shrink| {
                    (num % shrink) * (right.parse::<u64>().unwrap() % shrink)
                }),
                _ => panic!(),
            },
            (_, "old") => match operation[1] {
                "+" => Box::new(move |num, _shrink| left.parse::<u64>().unwrap() + num),
                "*" => Box::new(move |num, shrink| {
                    (left.parse::<u64>().unwrap() % shrink) * (num % shrink)
                }),
                _ => panic!(),
            },
            (_, _) => match operation[1] {
                "+" => Box::new(move |_num, _shrink| {
                    left.parse::<u64>().unwrap() + right.parse::<u64>().unwrap()
                }),
                "*" => Box::new(move |_num, _shrink| {
                    left.parse::<u64>().unwrap() * right.parse::<u64>().unwrap()
                }),
                _ => panic!(),
            },
        };
        let monkey = Monkey2 {
            items: line2
                .unwrap()
                .strip_prefix("  Starting items: ")
                .unwrap()
                .split(", ")
                .map(|num| num.parse::<u64>().unwrap())
                .collect::<VecDeque<u64>>(),
            function,
            denominator: line4
                .unwrap()
                .strip_prefix("  Test: divisible by ")
                .unwrap()
                .parse::<u64>()
                .unwrap(),
            true_monkey: line5
                .unwrap()
                .strip_prefix("    If true: throw to monkey ")
                .unwrap()
                .parse::<usize>()
                .unwrap(),
            false_monkey: line6
                .unwrap()
                .strip_prefix("    If false: throw to monkey ")
                .unwrap()
                .parse::<usize>()
                .unwrap(),
            items_inspected: 0,
        };
        lines.next();
        monkeys.push(monkey)
    }
    let mut moved_items = Vec::<VecDeque<u64>>::new();
    for _monkey in 0..monkeys.len() {
        moved_items.push(VecDeque::<u64>::new());
    }
    let mut top = 0;
    let mut second = 0;
    // let mut common_multiple = 1;
    // for monkey in &monkeys {
    //     common_multiple *= monkey.denominator;
    // }
    let mut divisors = Vec::<u64>::new();
    for monkey in &monkeys {
        divisors.push(monkey.denominator);
    }
    let common_multiple = lcm(divisors.as_slice());
    for _round in 1..=10000 {
        //for monkey in &mut monkeys {
        for monkey in 0..monkeys.len() {
            for item in &monkeys[monkey].items {
                //  Monkey inspects an item with a worry level of 79.
                // Worry level is multiplied by 19 to 1501.
                // Monkey gets bored with item. Worry level is divided by 3 to 500.
                // Current worry level is not divisible by 23.
                // Item with worry level 500 is thrown to monkey 3.
                let new_worry =
                    (monkeys[monkey].function)(*item, common_multiple) % common_multiple;
                if new_worry % monkeys[monkey].denominator == 0 {
                    let monkey_destination = monkeys[monkey].true_monkey;
                    moved_items[monkey_destination].push_back(new_worry);
                } else {
                    let monkey_destination = monkeys[monkey].false_monkey;
                    moved_items[monkey_destination].push_back(new_worry);
                }
            }
            monkeys[monkey].items_inspected += monkeys[monkey].items.len();
            monkeys[monkey].items.clear();
            for (monkey, item_list) in moved_items.iter_mut().enumerate() {
                for item in &mut *item_list {
                    monkeys[monkey].items.push_back(*item);
                }
                item_list.clear();
            }
        }
    }
    for monkey in monkeys {
        if monkey.items_inspected > top {
            second = top;
            top = monkey.items_inspected;
        } else if monkey.items_inspected > second {
            second = monkey.items_inspected;
        }
    }
    assert_eq!(top * second, 17673687232);
    println!("{}", top * second);
}

fn lcm(nums: &[u64]) -> u64 {
    if nums.len() == 1 {
        return nums[0];
    }
    let a = nums[0];
    let b = lcm(&nums[1..]);
    a * b / gcd_of_two_numbers(a, b)
}

fn gcd_of_two_numbers(a: u64, b: u64) -> u64 {
    if b == 0 {
        return a;
    }
    gcd_of_two_numbers(b, a % b)
}

fn day10() {
    let reader = load_puzzle("day10.txt");
    let mut current_cycle = 1;
    let mut x = 1;
    let mut signal_strength = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let input = line.split(' ').collect::<Vec<&str>>();
        let delta_x = match input[0] {
            "noop" => 0,
            "addx" => {
                // durring current cycle
                if (current_cycle - 20) % 40 == 0 && current_cycle <= 220 {
                    signal_strength += current_cycle * x;
                }
                let char = match (x - 1..=x + 1).contains(&((current_cycle - 1) % 40)) {
                    true => "#",
                    false => ".",
                };
                print!("{char}");
                if (current_cycle - 1) % 40 == 39 {
                    println!();
                }
                // durring current cycle

                // end current cycle
                current_cycle += 1;
                // end current cycle
                input[1].parse::<i32>().unwrap()
            }
            _ => panic!(),
        };
        // durring current cycle
        if (current_cycle - 20) % 40 == 00 && current_cycle <= 220 {
            signal_strength += current_cycle * x;
        }
        let char = match (x - 1..=x + 1).contains(&((current_cycle - 1) % 40)) {
            true => "#",
            false => ".",
        };
        print!("{char}");
        if (current_cycle - 1) % 40 == 39 {
            println!();
        }
        // durring current cycle

        // end current cycle
        x += delta_x;
        current_cycle += 1;
        // end current cycle
    }
    assert_eq!(signal_strength, 12740);
    println!("{signal_strength}");
}

fn day9() {
    let reader = load_puzzle("day9.txt");

    let mut h = (0, 0);
    let mut t = (0, 0);
    let mut visited = HashSet::new();
    visited.insert(t);

    for line in reader.lines() {
        let line = line.unwrap();
        let input = line.split(' ').collect::<Vec<&str>>();
        let h_diff = match input[0] {
            "L" => (-1, 0),
            "R" => (1, 0),
            "U" => (0, 1),
            "D" => (0, -1),
            _ => panic!(),
        };
        for _ in 1..=input[1].parse::<i32>().unwrap() {
            //println!("{:?}", input[0]);
            //println!("{:?}", h_diff);
            h.0 += h_diff.0;
            h.1 += h_diff.1;
            let x_diff: i32 = h.0 - t.0;
            let y_diff: i32 = h.1 - t.1;
            let diff_sum = x_diff.abs() + y_diff.abs();
            if x_diff.abs() >= 2 {
                if x_diff > 0 {
                    t.0 += x_diff - 1;
                } else {
                    t.0 += x_diff + 1;
                }

                if diff_sum == 3 {
                    t.1 += y_diff;
                }
            } else if y_diff.abs() >= 2 {
                if y_diff > 0 {
                    t.1 += y_diff - 1;
                } else {
                    t.1 += y_diff + 1;
                }

                if diff_sum == 3 {
                    t.0 += x_diff;
                }
            }
            visited.insert(t);
        }
    }
    assert_eq!(visited.len(), 6269);
    println!("{}", visited.len());

    let reader = load_puzzle("day9.txt");

    let mut t = [(0, 0); 10];
    let mut visited = HashSet::new();
    visited.insert(t[9]);

    for line in reader.lines() {
        let line = line.unwrap();
        let input = line.split(' ').collect::<Vec<&str>>();
        let h_diff = match input[0] {
            "L" => (-1, 0),
            "R" => (1, 0),
            "U" => (0, 1),
            "D" => (0, -1),
            _ => panic!(),
        };
        for _ in 1..=input[1].parse::<i32>().unwrap() {
            //println!("{:?}", input[0]);
            //println!("{:?}", h_diff);
            t[0].0 += h_diff.0;
            t[0].1 += h_diff.1;
            for i in 1..10 {
                let x_diff: i32 = t[i - 1].0 - t[i].0;
                let y_diff: i32 = t[i - 1].1 - t[i].1;
                let diff_sum = x_diff.abs() + y_diff.abs();
                if x_diff.abs() >= 2 {
                    if x_diff > 0 {
                        t[i].0 += x_diff - 1;
                    } else {
                        t[i].0 += x_diff + 1;
                    }

                    if diff_sum == 3 {
                        t[i].1 += y_diff;
                    }
                }
                if y_diff.abs() >= 2 {
                    if y_diff > 0 {
                        t[i].1 += y_diff - 1;
                    } else {
                        t[i].1 += y_diff + 1;
                    }

                    if diff_sum == 3 {
                        t[i].0 += x_diff;
                    }
                }
            }
            visited.insert(t[9]);
        }
    }
    assert_eq!(visited.len(), 2557);
    println!("{}", visited.len())
}

fn day8() {
    let reader = load_puzzle("day8.txt");
    const DIMENSIONS: usize = 99;
    let mut trees = [[Tree {
        right: Visible {
            visible: false,
            tallest: 0,
        },
        left: Visible {
            visible: false,
            tallest: 0,
        },
        top: Visible {
            visible: false,
            tallest: 0,
        },
        bottom: Visible {
            visible: false,
            tallest: 0,
        },
        height: 0,
        visible: false,
    }; DIMENSIONS]; DIMENSIONS];
    for (row, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        for (col, char) in line.chars().enumerate() {
            trees[row][col] = Tree {
                right: Visible {
                    visible: false,
                    tallest: 0,
                },
                left: Visible {
                    visible: false,
                    tallest: 0,
                },
                top: Visible {
                    visible: false,
                    tallest: 0,
                },
                bottom: Visible {
                    visible: false,
                    tallest: 0,
                },
                height: char.to_digit(10).unwrap(),
                visible: false,
            }
        }
    }
    let mut num_visible = (DIMENSIONS - 1) * 4;
    for i in 0..DIMENSIONS {
        trees[0][i].top = Visible {
            visible: true,
            tallest: trees[0][i].height,
        };
        trees[DIMENSIONS - 1][i].bottom = Visible {
            visible: true,
            tallest: trees[DIMENSIONS - 1][i].height,
        };
        trees[i][0].left = Visible {
            visible: true,
            tallest: trees[i][0].height,
        };
        trees[i][DIMENSIONS - 1].right = Visible {
            visible: true,
            tallest: trees[i][DIMENSIONS - 1].height,
        }
    }
    for a in 1..=DIMENSIONS - 2 {
        for b in 1..=DIMENSIONS - 2 {
            trees[a][b].top = Visible {
                visible: trees[a - 1][b].top.tallest < trees[a][b].height,
                tallest: max(trees[a - 1][b].top.tallest, trees[a][b].height),
            };
            if trees[a][b].top.visible && !trees[a][b].visible {
                trees[a][b].visible = true;
                num_visible += 1;
            }
            trees[b][a].left = Visible {
                visible: trees[b][a - 1].left.tallest < trees[b][a].height,
                tallest: max(trees[b][a - 1].left.tallest, trees[b][a].height),
            };
            if trees[b][a].left.visible && !trees[b][a].visible {
                trees[b][a].visible = true;
                num_visible += 1;
            }
        }
    }
    for a in (1..=DIMENSIONS - 2).rev() {
        for b in (1..=DIMENSIONS - 2).rev() {
            trees[a][b].bottom = Visible {
                visible: trees[a + 1][b].bottom.tallest < trees[a][b].height,
                tallest: max(trees[a + 1][b].bottom.tallest, trees[a][b].height),
            };
            if trees[a][b].bottom.visible && !trees[a][b].visible {
                trees[a][b].visible = true;
                num_visible += 1;
            }
            trees[b][a].right = Visible {
                visible: trees[b][a + 1].right.tallest < trees[b][a].height,
                tallest: max(trees[b][a + 1].right.tallest, trees[b][a].height),
            };
            if trees[b][a].right.visible && !trees[b][a].visible {
                trees[b][a].visible = true;
                num_visible += 1;
            }
        }
    }
    let a = 49;
    let b = 49;
    trees[a][b].top = Visible {
        visible: trees[a - 1][b].top.tallest < trees[a][b].height,
        tallest: max(trees[a - 1][b].top.tallest, trees[a][b].height),
    };
    if trees[a][b].top.visible && !trees[a][b].visible {
        trees[a][b].visible = true;
        num_visible += 1;
    }
    trees[b][a].left = Visible {
        visible: trees[b][a - 1].left.tallest < trees[b][a].height,
        tallest: max(trees[b][a - 1].left.tallest, trees[b][a].height),
    };
    if trees[b][a].left.visible && !trees[b][a].visible {
        trees[b][a].visible = true;
        num_visible += 1;
    }
    trees[a][b].bottom = Visible {
        visible: trees[a + 1][b].bottom.tallest < trees[a][b].height,
        tallest: max(trees[a + 1][b].bottom.tallest, trees[a][b].height),
    };
    if trees[a][b].bottom.visible && !trees[a][b].visible {
        trees[a][b].visible = true;
        num_visible += 1;
    }
    trees[b][a].right = Visible {
        visible: trees[b][a + 1].right.tallest < trees[b][a].height,
        tallest: max(trees[b][a + 1].right.tallest, trees[b][a].height),
    };
    if trees[b][a].right.visible && !trees[b][a].visible {
        trees[b][a].visible = true;
        num_visible += 1;
    }
    assert_eq!(num_visible, 1763);
    println!("{num_visible}");

    let reader = load_puzzle("day8.txt");
    //const DIMENSIONS: usize = 99;
    let mut trees = [[Tree2 {
        right: Visible2 { scores: [0; 10] },
        left: Visible2 { scores: [0; 10] },
        top: Visible2 { scores: [0; 10] },
        bottom: Visible2 { scores: [0; 10] },
        height: 0,
    }; DIMENSIONS]; DIMENSIONS];
    for (row, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        for (col, char) in line.chars().enumerate() {
            trees[row][col] = Tree2 {
                right: Visible2 { scores: [0; 10] },
                left: Visible2 { scores: [0; 10] },
                top: Visible2 { scores: [0; 10] },
                bottom: Visible2 { scores: [0; 10] },
                height: char.to_digit(10).unwrap() as usize,
            }
        }
    }
    //let mut num_visible = (DIMENSIONS-1)*4;
    // for i in 0..DIMENSIONS {
    //     trees[0][i].top = Visible2 {scores: [0; 0]};
    //     trees[DIMENSIONS-1][i].bottom = Visible {
    //         visible: true,
    //         tallest: trees[DIMENSIONS-1][i].height,
    //     };
    //     trees[i][0].left = Visible {
    //         visible: true,
    //         tallest: trees[i][0].height,
    //     };
    //     trees[i][DIMENSIONS-1].right = Visible {
    //         visible: true,
    //         tallest: trees[i][DIMENSIONS-1].height,
    //     }
    // }
    for a in 1..=DIMENSIONS - 2 {
        for b in 1..=DIMENSIONS - 2 {
            for h in 0..10usize {
                if h <= trees[a - 1][b].height {
                    trees[a][b].top.scores[h] = 1;
                } else {
                    trees[a][b].top.scores[h] = 1 + trees[a - 1][b].top.scores[h];
                }

                if h <= trees[b][a - 1].height {
                    trees[b][a].left.scores[h] = 1;
                } else {
                    trees[b][a].left.scores[h] = 1 + trees[b][a - 1].left.scores[h];
                }
            }
        }
    }
    for a in (1..=DIMENSIONS - 2).rev() {
        for b in (1..=DIMENSIONS - 2).rev() {
            for h in 0..10usize {
                if h <= trees[a + 1][b].height {
                    trees[a][b].bottom.scores[h] = 1;
                } else {
                    trees[a][b].bottom.scores[h] = 1 + trees[a + 1][b].bottom.scores[h];
                }

                if h <= trees[b][a + 1].height {
                    trees[b][a].right.scores[h] = 1;
                } else {
                    trees[b][a].right.scores[h] = 1 + trees[b][a + 1].right.scores[h];
                }
            }
        }
    }
    let a = 49;
    let b = 49;
    for h in 0..10usize {
        if h <= trees[a - 1][b].height {
            trees[a][b].top.scores[h] = 1;
        } else {
            trees[a][b].top.scores[h] = 1 + trees[a - 1][b].top.scores[h];
        }

        if h <= trees[b][a - 1].height {
            trees[b][a].left.scores[h] = 1;
        } else {
            trees[b][a].left.scores[h] = 1 + trees[b][a - 1].left.scores[h];
        }
    }
    for h in 0..10usize {
        if h <= trees[a + 1][b].height {
            trees[a][b].bottom.scores[h] = 1;
        } else {
            trees[a][b].bottom.scores[h] = 1 + trees[a + 1][b].bottom.scores[h];
        }

        if h <= trees[b][a + 1].height {
            trees[b][a].right.scores[h] = 1;
        } else {
            trees[b][a].right.scores[h] = 1 + trees[b][a + 1].right.scores[h];
        }
    }

    let mut max = 0;

    #[allow(clippy::needless_range_loop)]
    for a in 1..=DIMENSIONS - 2 {
        for b in 1..=DIMENSIONS - 2 {
            let height = trees[a][b].height;
            let score = trees[a][b].top.scores[height]
                * trees[a][b].left.scores[height]
                * trees[a][b].bottom.scores[height]
                * trees[a][b].right.scores[height];
            max = std::cmp::max(score, max);
        }
    }
    assert_eq!(max, 671160);
    println!("{max}");
}

fn day7() {
    let mut reader = load_puzzle("day7.txt");
    let mut skip = String::new();
    reader.read_line(&mut skip).unwrap();

    let mut fs = BTreeMap::new();
    let mut current_dir = PathBuf::from(r"/");

    let mut dirs_to_check = HashSet::new();
    let mut contains_only_files = false;
    let mut ls_called_in_dir = false;
    let mut size_of_files = 0;
    let mut number_of_sub_dirs = 0;

    fs.insert(
        current_dir.clone(),
        Directory {
            total_size: 0,
            sub_dirs_unaccounted_for: 0,
        },
    );

    for line in reader.lines() {
        let line = line.unwrap();
        let chunks = line.split(' ').collect::<Vec<&str>>();
        match chunks[0] {
            "dir" => {
                contains_only_files = false;
                number_of_sub_dirs += 1;
            }
            "$" => match chunks[1] {
                "ls" => {
                    contains_only_files = true;
                    ls_called_in_dir = true;
                }
                "cd" => {
                    if contains_only_files {
                        dirs_to_check.insert(current_dir.clone());
                        contains_only_files = false;
                    }
                    if ls_called_in_dir {
                        fs.entry(current_dir.clone()).and_modify(|dir| {
                            dir.total_size = size_of_files;
                            dir.sub_dirs_unaccounted_for = number_of_sub_dirs
                        });
                        ls_called_in_dir = false;
                    }
                    size_of_files = 0;
                    number_of_sub_dirs = 0;
                    if chunks[2] == ".." {
                        current_dir.pop();
                    } else {
                        current_dir.push(chunks[2]);
                        if !fs.contains_key(&current_dir) {
                            fs.insert(
                                current_dir.clone(),
                                Directory {
                                    total_size: 0,
                                    sub_dirs_unaccounted_for: 0,
                                },
                            );
                        }
                    }
                }
                _ => panic!(),
            },
            _ => {
                let size = chunks[0].parse::<usize>().unwrap();
                size_of_files += size;
                let _name = chunks[1];
            }
        }
    }
    if contains_only_files {
        dirs_to_check.insert(current_dir.clone());
    }
    if ls_called_in_dir {
        fs.entry(current_dir).and_modify(|dir| {
            dir.total_size = size_of_files;
            dir.sub_dirs_unaccounted_for = number_of_sub_dirs
        });
    }

    let mut dirs_to_check_temp = HashSet::new();
    let mut sum = 0;
    let mut sizes = BTreeMap::new();
    let mut total_used = 0;
    let mut parent;
    loop {
        for dir_to_check in dirs_to_check.iter() {
            let size = fs[dir_to_check].total_size;
            if size < 100_000 {
                sum += size;
            }
            sizes.insert(size, ());
            if *dir_to_check == PathBuf::from(r"/") {
                total_used = size;
            }

            parent = dir_to_check.clone();
            parent.pop();
            if parent == *dir_to_check {
                break;
            }

            fs.entry(parent.clone()).and_modify(|dir| {
                dir.total_size += size;
                dir.sub_dirs_unaccounted_for -= 1
            });

            if fs[&parent].sub_dirs_unaccounted_for == 0 {
                dirs_to_check_temp.insert(parent);
            }
        }
        dirs_to_check = dirs_to_check_temp;
        dirs_to_check_temp = HashSet::new();
        if dirs_to_check.is_empty() {
            break;
        }
    }
    let amount_to_free = 30000000 - (70000000 - total_used);
    let found = sizes.range(amount_to_free..).next().unwrap().0;
    assert_eq!(sum, 1453349);
    assert_eq!(*found, 2948823);
    println!("{sum} {found}")
}

fn day6() {
    let mut reader = load_puzzle("day6.txt");
    let mut signal = String::new();
    let mut dup_finder = Processor {
        current_letters: Default::default(),
        letters_set: Default::default(),
        letter_map: Default::default(),
    };
    reader.read_line(&mut signal).unwrap();
    let mut signal_iter = signal.chars();
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    let mut location = 4;
    for letter in signal_iter {
        if dup_finder.count_unique() == 4 {
            break;
        }
        location += 1;
        dup_finder.push(letter);
        dup_finder.pop();
    }
    assert_eq!(location, 1658);
    println!("{location}");

    let mut reader = load_puzzle("day6.txt");
    let mut signal = String::new();
    let mut dup_finder = Processor {
        current_letters: Default::default(),
        letters_set: Default::default(),
        letter_map: Default::default(),
    };
    reader.read_line(&mut signal).unwrap();
    let mut signal_iter = signal.chars();
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    dup_finder.push(signal_iter.next().unwrap());
    let mut location = 14;
    for letter in signal_iter {
        if dup_finder.count_unique() == 14 {
            break;
        }
        location += 1;
        dup_finder.push(letter);
        dup_finder.pop();
    }
    assert_eq!(location, 2260);
    println!("{location}");
}

fn day5() {
    let mut reader = load_puzzle("day5.txt");
    let mut array: [String; 8] = Default::default();
    for string in array.iter_mut() {
        reader.read_line(string).unwrap();
    }
    let mut empty = String::new();
    reader.read_line(&mut empty).unwrap();
    reader.read_line(&mut empty).unwrap();

    let mut columns: [VecDeque<char>; 9] = Default::default();

    for string in array.iter().rev() {
        let chars = string.chars().collect::<Vec<char>>();
        for (n, column) in columns.iter_mut().enumerate() {
            let char = chars[n * 4 + 1];
            if char != ' ' {
                column.push_front(char);
            }
        }
    }

    for line in reader.lines() {
        let line = line.unwrap();
        let line = line.split(' ').collect::<Vec<&str>>();
        let num = line[1].parse::<i32>().unwrap();
        let source = line[3].parse::<usize>().unwrap() - 1;
        let destination = line[5].parse::<usize>().unwrap() - 1;
        for _n in 1..=num {
            let to_move = columns[source].pop_front().unwrap();
            columns[destination].push_front(to_move);
        }
    }
    let mut output = "".to_string();
    for column in columns.iter() {
        let top = column.front().unwrap();
        output += &top.to_string();
        print!("{top}");
    }
    assert_eq!(output, "FZCMJCRHZ");
    println!();

    let mut reader = load_puzzle("day5.txt");
    let mut array: [String; 8] = Default::default();
    for string in array.iter_mut() {
        reader.read_line(string).unwrap();
    }
    let mut empty = String::new();
    reader.read_line(&mut empty).unwrap();
    reader.read_line(&mut empty).unwrap();

    let mut columns: [Vec<char>; 9] = Default::default();

    for string in array.iter().rev() {
        let chars = string.chars().collect::<Vec<char>>();
        for (n, column) in columns.iter_mut().enumerate() {
            let char = chars[n * 4 + 1];
            if char != ' ' {
                column.push(char);
            }
        }
    }

    for line in reader.lines() {
        let line = line.unwrap();
        let line = line.split(' ').collect::<Vec<&str>>();
        let num = line[1].parse::<usize>().unwrap();
        let source = line[3].parse::<usize>().unwrap() - 1;
        let destination = line[5].parse::<usize>().unwrap() - 1;
        let to_move = columns[source]
            .drain(columns[source].len() - num..columns[source].len())
            .collect::<Vec<char>>();
        columns[destination].extend(to_move);
    }
    let mut output = "".to_string();
    for column in columns.iter() {
        let top = column.last().unwrap();
        output += &top.to_string();
        print!("{top}");
    }
    assert_eq!(output, "JSDHQMZGF");
    println!()
}

fn day4() {
    let reader = load_puzzle("day4.txt");
    let mut count = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let sections = line.split(',').collect::<Vec<&str>>();
        let section1 = sections[0]
            .split('-')
            .map(|num| num.parse::<i32>().unwrap())
            .collect::<Vec<i32>>();
        let section2 = sections[1]
            .split('-')
            .map(|num| num.parse::<i32>().unwrap())
            .collect::<Vec<i32>>();
        match section1[0].cmp(&section2[0]) {
            Ordering::Greater => {
                if section1[1] <= section2[1] {
                    count += 1;
                }
            }
            Ordering::Less => {
                if section1[1] >= section2[1] {
                    count += 1;
                }
            }
            Ordering::Equal => count += 1,
        };
    }
    assert_eq!(count, 459);
    println!("{count}");

    let reader = load_puzzle("day4.txt");
    let mut count = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let sections = line.split(',').collect::<Vec<&str>>();
        let section1 = sections[0]
            .split('-')
            .map(|num| num.parse::<i32>().unwrap())
            .collect::<Vec<i32>>();
        let section2 = sections[1]
            .split('-')
            .map(|num| num.parse::<i32>().unwrap())
            .collect::<Vec<i32>>();
        match section1[0].cmp(&section2[0]) {
            Ordering::Greater => {
                if section1[0] <= section2[1] {
                    count += 1;
                }
            }
            Ordering::Less => {
                if section2[0] <= section1[1] {
                    count += 1;
                }
            }
            Ordering::Equal => count += 1,
        };
    }
    assert_eq!(count, 779);
    println!("{count}")
}

fn day3() {
    let reader = load_puzzle("day3.txt");
    let mut total_priority = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let (first, second) = line.split_at(line.len() / 2);
        let first: HashSet<char, RandomState> = HashSet::from_iter(first.chars());
        let second = HashSet::from_iter(second.chars());
        for letter in first.intersection(&second) {
            total_priority += get_priority(letter);
        }
    }
    assert_eq!(total_priority, 7691);
    println!("{total_priority}");
    let reader = load_puzzle("day3.txt");
    let mut lines: [String; 3] = ["1".to_string(), "2".to_string(), "3".to_string()];
    let mut total_priority = 0;
    for (line_index, line) in reader.lines().enumerate() {
        lines[line_index % 3] = line.unwrap();
        if line_index % 3 == 2 {
            let first: HashSet<char, RandomState> = HashSet::from_iter(lines[0].chars());
            let second = HashSet::from_iter(lines[1].chars());
            let intersection_1_2 = &first & &second; // this is inefficient and requires a copy of some sort but I don't want to make the fast version
            let third = HashSet::from_iter(lines[2].chars());
            for letter in intersection_1_2.intersection(&third) {
                total_priority += get_priority(letter);
            }
        }
    }
    assert_eq!(total_priority, 2508);
    println!("{total_priority}")
}

fn get_priority(letter: &char) -> i32 {
    match letter {
        'a' => 1,
        'b' => 2,
        'c' => 3,
        'd' => 4,
        'e' => 5,
        'f' => 6,
        'g' => 7,
        'h' => 8,
        'i' => 9,
        'j' => 10,
        'k' => 11,
        'l' => 12,
        'm' => 13,
        'n' => 14,
        'o' => 15,
        'p' => 16,
        'q' => 17,
        'r' => 18,
        's' => 19,
        't' => 20,
        'u' => 21,
        'v' => 22,
        'w' => 23,
        'x' => 24,
        'y' => 25,
        'z' => 26,
        'A' => 27,
        'B' => 28,
        'C' => 29,
        'D' => 30,
        'E' => 31,
        'F' => 32,
        'G' => 33,
        'H' => 34,
        'I' => 35,
        'J' => 36,
        'K' => 37,
        'L' => 38,
        'M' => 39,
        'N' => 40,
        'O' => 41,
        'P' => 42,
        'Q' => 43,
        'R' => 44,
        'S' => 45,
        'T' => 46,
        'U' => 47,
        'V' => 48,
        'W' => 49,
        'X' => 50,
        'Y' => 51,
        'Z' => 52,
        _ => 0,
    }
}

fn day2() {
    let reader = load_puzzle("day2.txt");

    let mut points = 0;
    for line in reader.lines() {
        match line.unwrap().as_str() {
            "A X" => points += 4,
            "A Y" => points += 8,
            "A Z" => points += 3,
            "B X" => points += 1,
            "B Y" => points += 5,
            "B Z" => points += 9,
            "C X" => points += 7,
            "C Y" => points += 2,
            "C Z" => points += 6,
            _ => (),
        }
    }
    assert_eq!(points, 9177);
    println!("{points}");

    let reader = load_puzzle("day2.txt");

    let mut points = 0;
    for line in reader.lines() {
        match line.unwrap().as_str() {
            "A X" => points += 3,
            "A Y" => points += 4,
            "A Z" => points += 8,
            "B X" => points += 1,
            "B Y" => points += 5,
            "B Z" => points += 9,
            "C X" => points += 2,
            "C Y" => points += 6,
            "C Z" => points += 7,
            _ => (),
        }
    }
    assert_eq!(points, 12111);
    println!("{points}")
}

fn day1() {
    let reader = load_puzzle("day1.txt");

    let mut top_three = TopThree {
        first: 0,
        second: 0,
        third: 0,
    };
    let mut count = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        if line.is_empty() {
            top_three.add(count);
            count = 0;
        } else {
            count += line.parse::<i32>().unwrap();
        }
    }
    let max = top_three.first;
    let sum = top_three.sum();
    assert_eq!(max, 71471);
    assert_eq!(sum, 211189);
    println!("{max}, {sum}")
}

fn load_puzzle(name: &str) -> BufReader<File> {
    let mut dir = env::current_exe().unwrap();
    dir.pop();
    dir.pop();
    dir.pop();
    dir.push("puzzles");
    dir.push(name);
    let file = File::open(dir).unwrap();
    BufReader::new(file)
}
